;
; Copyright (c) 2016, RISC OS Open Ltd
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of RISC OS Open Ltd nor the names of its contributors
;       may be used to endorse or promote products derived from this software
;       without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
;

        IMPORT  memcpy

        GET     Hdr:ListOpts
        GET     Hdr:CPU.Arch
        GET     Hdr:Macros
        GET     Hdr:Proc
        GET     BCM2835.hdr
        GET     StaticWS.hdr

sb      RN      9

        EXPORT  DBell_InitDevices

        AREA    |Asm$$Code|, CODE, READONLY

DBell_InitDevices ROUT
        CPUDetect a1
        MOVCC   pc, lr
        Entry
        ADRL    a1, DBellDevice
        ADR     a2, DBellTemplate
        MOV     a3, #HALDevice_DBell_Size
        BL      memcpy
        ADRL    a2, DBellDevice
        LDR     a1, IntBase
        ADD     a1, a1, #QA7_CORE0_MBOX0_RDCLR
        STR     a1, [a2, #HALDevice_Address]
        MOV     a1, #0
        MOV     lr, pc
        LDR     pc, OSentries+4*OS_AddDevice
        EXIT

        MACRO
$class  HALDeviceField $field, $value
        LCLS    myvalue
      [ "$value" = ""
myvalue SETS    "$field"
      |
myvalue SETS    "$value"
      ]
        ASSERT  . - %A0 = HALDevice_$class$field
     [ ?HALDevice_$class$field = 2
        DCW     $myvalue
   ELIF ?HALDevice_$class$field = 4
        DCD     $myvalue
      |
        %       ?HALDevice_$class$field
      ]
        MEND

DBellTemplate
0
        HALDeviceField Type,               HALDeviceType_Comms + HALDeviceComms_ARMDBell
        HALDeviceField ID,                 HALDeviceID_ARMDBell_BCM2836
        HALDeviceField Location,           HALDeviceBus_Sys + HALDeviceSysBus_AHB ; Guess
        HALDeviceField Version,            0
        HALDeviceField Description,        DBell_Description
        HALDeviceField Address,            0
        HALDeviceField Reserved1,          0
        HALDeviceField Activate,           DBell_Activate
        HALDeviceField Deactivate,         DBell_Deactivate
        HALDeviceField Reset,              DBell_Reset
        HALDeviceField Sleep,              DBell_Sleep
        HALDeviceField Device,             -1 ; IRQ numbers vary by core, use DBell_GetIRQ
        HALDeviceField TestIRQ,            DBell_TestIRQ
        HALDeviceField ClearIRQ,           DBell_ClearIRQ
        HALDeviceField Reserved2,          0
        ASSERT  . - %A0 = HALDeviceSize
DBell   HALDeviceField GetIRQ,             DBell_GetIRQ
DBell   HALDeviceField Ring,               DBell_Ring
        ASSERT  . - %A0 = HALDevice_DBell_Size

DBell_Description
        DCB     "BCM2836/2837 QA7 mailbox 0 doorbell", 0
        ALIGN

DBell_Activate
DBell_TestIRQ
        MOV     a1, #1
DBell_Deactivate
DBell_Reset
        MOV     pc, lr

DBell_Sleep
        MOV     a1, #0
        MOV     pc, lr

DBell_GetIRQ
        ; In: a2 = core number
        ; Return GIC or non-GIC device numbers, as appropriate
        CPUDetect ip
        ADDLS   a1, a2, #iDev_QA7_MBox0
        MOVHI   a1, a2, LSL #2
        ADDHI   a1, a1, #iDev_GIC_QA7_Core0_MBox0
        MOV     pc, lr

DBell_ClearIRQ
        LDR     a1, [a1, #HALDevice_Address] ; -> QA7_CORE0_MBOX0_RDCLR
        MRC     p15, 0, a2, c0, c0, 5
        MVN     a3, #0
        AND     a2, a2, #3
        ASSERT  QA7_CORE1_MBOX0_RDCLR-QA7_CORE0_MBOX0_RDCLR = 16
        STR     a3, [a1, a2, LSL #4]
        MOV     pc, lr

DBell_Ring
        ; In: a2 = mask of CPU cores
        LDR     a1, [a1, #HALDevice_Address]
        TST     a2, #1
        STRNE   a1, [a1, #QA7_CORE0_MBOX0_SET-QA7_CORE0_MBOX0_RDCLR]
        TST     a2, #2
        STRNE   a1, [a1, #QA7_CORE1_MBOX0_SET-QA7_CORE0_MBOX0_RDCLR]
        TST     a2, #4
        STRNE   a1, [a1, #QA7_CORE2_MBOX0_SET-QA7_CORE0_MBOX0_RDCLR]
        TST     a2, #8
        STRNE   a1, [a1, #QA7_CORE3_MBOX0_SET-QA7_CORE0_MBOX0_RDCLR]
        MOV     pc, lr

        END

