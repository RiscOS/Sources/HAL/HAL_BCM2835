;
; Copyright (c) 2015, RISC OS Open Ltd
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of RISC OS Open Ltd nor the names of its contributors
;       may be used to endorse or promote products derived from this software
;       without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
;

        AREA    |ARM$$code|, CODE, READONLY

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:Proc
        GET     Hdr:System
        GET     Hdr:FSNumbers
        GET     Hdr:NewErrors
        GET     Hdr:BCMSupport
        GET     BCM2835.hdr
        GET     StaticWS.hdr
        GET     CastleMacros.hdr

        EXPORT  Touch_InitDevices

        IMPORT  memcpy
        IMPORT  GetVCBuffer

        MACRO
$class  HALDeviceField $field, $value
        LCLS    myvalue
      [ "$value" = ""
myvalue SETS    "$field"
      |
myvalue SETS    "$value"
      ]
        ASSERT  . - %A0 = HALDevice_$class$field
     [ ?HALDevice_$class$field = 2
        DCW     $myvalue
   ELIF ?HALDevice_$class$field = 4
        DCD     $myvalue
      |
        %       ?HALDevice_$class$field
      ]
        MEND


; Template for touchscreen device

Touch_Dev
0
        HALDeviceField Type,               HALDeviceType_HID + HALDeviceHID_Touchscreen
        HALDeviceField ID,                 HALDeviceID_Touchscreen_FT5406
        HALDeviceField Location,           HALDeviceBus_Sys + HALDeviceSysBus_AHB ; Guess
        HALDeviceField Version,            0
        HALDeviceField Description,        Touch_Description
        HALDeviceField Address,            0 ; Filled in later
        HALDeviceField Reserved1,          0
        HALDeviceField Activate,           Touch_Activate
        HALDeviceField Deactivate,         Touch_Deactivate
        HALDeviceField Reset,              Touch_Reset
        HALDeviceField Sleep,              Touch_Sleep
        HALDeviceField Device,             -1
        HALDeviceField TestIRQ,            0
        HALDeviceField ClearIRQ,           0
        HALDeviceField Reserved2,          0
        ASSERT  . - %A0 = HALDeviceSize

Touch_Description
        = "FT5406 Touch Panel Controller", 0

        ALIGN

        ; Initialise our HAL devices
Touch_InitDevices ROUT
        Entry   "v1"
        ; Try and initialise the register softcopy buffer
        LDR     a1, =ARM2VC_Tag_FBGetTouchBuf
        MOV     a2, #256
        LDR     a3, =ARM2VC_Tag_FBSetTouchBuf
        BL      GetVCBuffer
        MOVS    v1, a1
        EXIT    EQ
        ADRL    a1, TouchDevice
        ADR     a2, Touch_Dev
        MOV     a3, #HALDeviceSize
        BL      memcpy
        ADRL    a2, TouchDevice
        STR     v1, [a2, #HALDevice_Address]
        ; Register the device
        MOV     a1, #0
        CallOS  OS_AddDevice
        EXIT

Touch_Activate
        MOV     a1, #1
Touch_Deactivate
Touch_Reset
        MOV     pc, lr

Touch_Sleep
        MOV     a1, #0
        MOV     pc, lr

        END
